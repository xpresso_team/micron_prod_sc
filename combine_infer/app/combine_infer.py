from __future__ import print_function

import math
import operator

import numpy as np
import os
from keras.preprocessing.sequence import pad_sequences
from keras.models import load_model
from app.create_filename import create_filename
import json
from flask import Flask, request, jsonify, render_template, Response
import pickle
from xpresso.ai.core.logging.xpr_log import XprLogger

cur_work_dir = os.getcwd()
app = Flask(__name__, template_folder="../scripts/templates", static_folder="../scripts/static")

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

use_gpu_bool = config['lstm']['use_gpu']
num_of_gpu = len(config['lstm']['cuda_visible_devices'].split(","))

if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    if num_of_gpu == 1:
        config['lstm']['use_gpu'] = False
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)

''' Creating logger '''
path = os.getcwd()
config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
my_logger = XprLogger(config_path=config_file)


def load_model_pg_bn(filename_pg_bn):
    """ Loading graph """
    input_dict = open(filename_pg_bn, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


def load_test_file_helper_combine(range_list, split, is_bn):
    master_dict = {}
    edges = {}
    for item in range_list:
        my_logger.info("Loading file %s", str(item))
        print("Loading file", str(item))
        filename_pg_bn = split.replace('VAL', str(item))
        if is_bn:
            master_dict[str(item)] = load_model_pg_bn(filename_pg_bn)
        else:
            edges.update(load_model_pg_bn(filename_pg_bn))
    if is_bn:
        return master_dict
    else:
        return edges


def get_pred(key_val, item3, modeldata):
    """ Getting prediction of sequence """
    pred = []
    key = '_'.join([str(item2) for item2 in key_val])
    print('Key', key)
    if key in modeldata[item3]:
        pred = modeldata[item3][key]
    print('pred', pred)
    return pred


def get_model_output(test_seq, number, lookahead, modeldata, weight):
    """ Get sequence and return the predicted sequence """
    test_seq_final = test_seq[-lookahead:]
    weight_predict = weight[1 - len(test_seq_final):] + [1]
    predict_dict = {}
    for ind, seq in enumerate(test_seq_final):
        if seq in modeldata:
            for block, prob in modeldata[seq][:number]:
                predict_dict[block] = round(prob * weight_predict[ind], 5)
    predict_dict_sorted = sorted(predict_dict.items(), key=operator.itemgetter(1), reverse=True)
    predicted = [item[0] for item in predict_dict_sorted][:number]
    return predicted


def get_model_output_bn(block, neighbor, lookahead, range_list, modeldata):
    """ Get sequence and return the predicted sequence """
    if len(block) < lookahead:
        block = [-1] * (lookahead - len(block)) + block
    else:
        block = block[-lookahead:]

    predict = []
    for item in range_list:
        key = list(block)[-item:]
        predict += get_pred(key, str(item), modeldata)
        if len(predict) >= neighbor:
            break
    return predict[:neighbor]


def pg_inference(request_body):
    data_model = config['model']['pg_bn']
    mountedpath = config['data_preprocess']['mounted_path'] + master_path
    modelpath = mountedpath + data_model['model_path_pg']

    lookahead = data_model['lookahead_window']
    weight = [round(math.exp(val * -0.65), 5) for val in range(lookahead - 1, 0, -1)]
    range_list = range(1, lookahead + 1)
    pg_split = modelpath + data_model['output_files']['pg_split']

    my_logger.info("Process started")
    print("Process started")

    my_logger.info("Model loading... ")
    print("Model loading... ")
    modeldata = load_test_file_helper_combine(range_list, pg_split, False)
    my_logger.info("Model loading Completed")
    print("Model loading Completed")

    try:
        block = request_body['InputSequence']
        neighbor = int(request_body['Topk'])
        block = block.split()
        my_logger.info(f'Input {block}')
        my_logger.info(f'TopK {neighbor}')
        resp_seq = get_model_output(block, neighbor, lookahead, modeldata, weight)
        my_logger.info(f'Output {resp_seq}')
        print(f'Block_Sequence: {resp_seq}')
    except Exception:
        final_output = "Internal Server Error"
        my_logger.info('Output %s', final_output)
        return {'error': final_output}

    return {"prediction": resp_seq}


def bn_inference(request_body):
    data_model = config['model']['pg_bn']
    mountedpath = config['data_preprocess']['mounted_path'] + master_path
    modelpath = mountedpath + data_model['model_path_bn']
    lookahead = data_model['lookahead_window']
    range_list = range(lookahead, 0, -1)
    bn_split = modelpath + data_model['output_files']['bn_split']

    my_logger.info("Process started")
    print("Process started")

    my_logger.info("Model loading... ")
    print("Model loading... ")
    modeldata = load_test_file_helper_combine(range_list, bn_split, True)
    my_logger.info("Model loading Completed")
    print("Model loading Completed")

    try:
        block = request_body['InputSequence']
        neighbor = int(request_body['Topk'])
        block = block.split()
        my_logger.info('Input %s', block)
        my_logger.info(f'TopK {neighbor}')
        resp_seq = get_model_output_bn(block, neighbor, lookahead, range_list, modeldata)
        my_logger.info(f'Output {resp_seq}')
        print(f'Block_Sequence: {resp_seq}')

    except Exception:
        final_output = "Internal Server Error"
        my_logger.info('Output %s', final_output)
        return {'error': final_output}
    return {"prediction": resp_seq}


def lstm_inference(request_body):
    my_logger.info("Loading Tokenizer...")
    # Loading tokenizer
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    index_to_id = {v: k for k, v in tokenizer.word_index.items()}
    my_logger.info("Tokenizer Loading Completed")

    # Loading max length
    my_logger.info("Loading Max Sequence Length...")
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
        max_length = pickle.load(handle)
    handle.close()
    my_logger.info("Max Sequence Length Loading Completed")

    # Loading model
    my_logger.info("Loading model ...")
    model_name = "model" + create_filename(config) + ".h5"
    print("Model Name", model_name)
    if os.path.exists(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   config['lstm']['model_save_path'], model_name))):
        model = load_model(os.path.abspath(
            os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                         str(config['lstm']['model_save_path']), str(model_name))))

        use_gpu_bool = config["lstm"]["use_gpu"]
    my_logger.info("Model loading completed")
    try:
        input_sequence = request_body['InputSequence']
        k = int(request_body['Topk'])
        input_sequence = input_sequence.split()
        input_sequence_tokenized = tokenizer.texts_to_sequences([input_sequence])[0]
        input_sequence_padded = pad_sequences([input_sequence_tokenized], maxlen=max_length - 1, padding='pre')

        predicted_ids = np.ndarray.tolist(np.argsort(model.predict(input_sequence_padded), axis=1))[0]
        predicted_ids.reverse()
        resp_seq = list()

        for index, value in enumerate(predicted_ids):
            if index >= k:
                break
            resp_seq.append(index_to_id[value])
        return {"prediction": resp_seq}

    except Exception as e:
        return {'error': str(e)}


@app.route("/")
def home():
    return render_template('form.html')


@app.route("/combine_infer", methods=["POST"])
def combine_infer():
    request_body = request.get_json()
    my_logger.info("Request Body : " + str(request_body) + "\n")
    if 'InputSequence' not in request.get_json() or 'Topk' not in request.get_json() or 'Model' not in request.get_json():
        return Response(
            status=404,
            response="Required Parameters not found"
        )

    if request_body['Model'] == 'lstm':
        resp_seq = lstm_inference(request_body)
    elif request_body['Model'] == 'pg':
        resp_seq = pg_inference(request_body)
    elif request_body['Model'] == 'bn':
        resp_seq = bn_inference(request_body)
    else:
        resp_seq = "Invalid Parameter Model"
    return Response(
        status=200,
        response=json.dumps({"predicted sequences": resp_seq})
    )


def main():
    app.run(host='0.0.0.0', port=8000, threaded=False, debug=False, use_reloader=False)


if __name__ == "__main__":
    main()
