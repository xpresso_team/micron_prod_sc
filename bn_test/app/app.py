""" Bayesian network module """
import json
import logging
import os
import pickle
from nltk import ngrams
import pandas as pd
import numpy as np
from xpresso.ai.core.logging.xpr_log import XprLogger


# Folder creating function
def create_folders(dirnames, pathname):
    """ Creating folder if not present """
    for dirname in dirnames:
        folder_name = pathname + dirname
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)


# Model loading function
def load_model(filename):
    """ Loading graph """
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


# Bayesian network class
class BNetworkTest(object):
    """ Declaring and defining class objects """

    def __init__(self):
        """ Initialising variables """

        # Reading config data from json
        self.config_path = 'config/config.json'
        self.config_file = open(self.config_path, 'r')
        self.json_object = json.load(self.config_file)
        self.config_file.close()

        self.data_prep_param = self.json_object['data_preprocess']
        self.data_model = self.json_object['model']['pg_bn']

        self.mountedpath = self.data_prep_param['mounted_path']
        self.inputfile = self.data_prep_param['input_file']

        self.cutting_window = self.data_prep_param['cutting_window']
        self.read_write_flag = self.data_prep_param['use_read_write_operation_feature']
        self.splitperc = self.data_prep_param['train_split_ratio']
        self.folders = self.data_prep_param['folders']

        self.filename = str(self.inputfile.split('/')[-1].split('.')[0])
        self.master_path = self.data_prep_param['master_path']
        self.master_path = self.master_path.replace("FILE", self.filename)
        self.master_path = self.master_path.replace("CUTTING", str(self.cutting_window))
        self.master_path = self.master_path.replace("SPLIT", str(self.splitperc))
        self.master_path = self.master_path.replace("RW", str(self.read_write_flag))
        self.mountedpath = self.data_prep_param['mounted_path'] + self.master_path

        self.datapath = self.mountedpath + self.data_prep_param['data_path']
        self.validatefileseq = self.datapath + self.data_prep_param['validate_file_seq']

        self.lookahead = self.data_model['lookahead_window']
        self.neighbors = self.data_model['topk_neighbor_predict_counts']
        self.modelpath = self.mountedpath + self.data_model['model_path_bn']
        self.resultfile = self.modelpath + self.data_prep_param['validate_result']
        self.logpath = self.modelpath + self.data_prep_param['logfile_test']

        self.bn_split = self.modelpath + self.data_model['output_files']['bn_split']

        self.range_list = range(self.lookahead, 0, -1)
        self.lines_test = []
        self.master_dict = {}

        self.test_seq_all = []
        self.act_ans_all = []
        self.pred_all = []
        self.hit_miss_all = []
        self.my_logger = logging.getLogger()

    def get_pred(self, key_val, item3):
        """ Getting prediction of sequence """
        pred = []
        key = '_'.join([str(item2) for item2 in key_val])
        if key in self.master_dict[item3]:
            pred = self.master_dict[item3][key]
        return pred

    def load_test_file_helper(self):
        """ helper function for test sequence generation """
        for item in self.range_list:
            self.my_logger.info("%s", item)
            print("Loading model files ", str(item))
            filename = self.bn_split.replace('VAL', str(item))
            self.master_dict[str(item)] = load_model(filename)
        return self.master_dict['1']

    def load_test_file(self):
        """ Reading test file and dividing block access into sequences """
        active_list = self.load_test_file_helper()
        with open(self.validatefileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                total_records = line_num
        with open(self.validatefileseq) as file_obj:
            for line_num, line in enumerate(file_obj):
                if not (line_num + 1) % 1000000:
                    loading_val = round(float(line_num) * 100 / float(total_records + 1), 2)
                    self.my_logger.info("loading %s ", str(loading_val))
                    print("loading test data", str(loading_val))
                validate_seq = line.replace("\n", "").split(" ")
                # validate_seq = [item for item in validate_seq if item in active_list]
                if len(validate_seq) <= 1:
                    continue
                if validate_seq:
                    self.lines_test.append(validate_seq)

    def test(self, neighbors):
        """ Calculating hit ratio """
        arrlist, hit_all, total_all = [], 0, 0
        for line_num, lines in enumerate(self.lines_test):
            if (line_num + 1) % 100000 == 0:
                output_string = "getting ngram for test "
                output_string += str(round(line_num * 100.0 / len(self.lines_test), 2))
                self.my_logger.info("%s", output_string)
                print(output_string)
            lines = [-1] * (self.lookahead) + lines
            arrlist += list(ngrams(lines, self.lookahead + 1))
        for line_num, item1 in enumerate(arrlist):
            hit_temp = 0
            if not (line_num + 1) % 1000:
                output_string = "testing "
                output_string += str(round(float(line_num) * 100 / float(len(arrlist)), 2)) + " "
                output_string += str(round(float(hit_all) * 100.0 / float(total_all), 2)) + " "
                output_string += str(neighbors)
                self.my_logger.info("%s", output_string)
                print(output_string)
            predict = []
            val = str(list(item1)[-1])
            key_master = list(item1)[:-1]
            key_temp = [item for item in key_master if item != -1]
            if not key_temp:
                continue
            for item3 in self.range_list:
                key = key_master[-item3:]
                predict += self.get_pred(key, str(item3))
                if len(predict) >= neighbors:
                    break
            predicted = predict[:neighbors]
            if val in predicted:
                hit_temp = 1
            total_all += 1
            hit_all += hit_temp
            self.test_seq_all.append(key_temp)
            self.act_ans_all.append(val)
            self.hit_miss_all.append(hit_temp)
            if not predicted:
                self.pred_all.append(-1)
            else:
                self.pred_all.append(predicted[0])
        if total_all:
            ratio = round(float(hit_all) * 100.0 / float(total_all), 2)
        else:
            ratio = 0.0
        output_string = "Neighbor: " + str(neighbors)
        output_string += " Lookback_window: " + str(self.lookahead)
        output_string += " Cutting_window: " + str(self.cutting_window) + "\n"
        output_string += "Hit: " + str(hit_all)
        output_string += " Total: " + str(total_all)
        output_string += " Ratio: " + str(ratio) + "\n"
        self.my_logger.info("%s", output_string)
        pred_data = {'test': self.test_seq_all, 'act': self.act_ans_all,
                     'pred': self.pred_all, 'hit': self.hit_miss_all}
        pred_data_frame = pd.DataFrame(pred_data)
        pred_data_frame.to_csv(self.resultfile, index=False, header=True)
        print(f"Run complete, Hit ratio {str(ratio)} %")

    def get_estimated_time(self):
        """ Getting estimated time """
        filesize = 0
        for item in self.range_list:
            filename = self.bn_split.replace('VAL', str(item))
            filesize += np.ceil(os.stat(filename).st_size / 1024.0 ** 2)
        estimated_time = np.ceil(filesize / (4500 * 60.0))
        output_string = "Estimated Time " + str(estimated_time) + "mins"
        print(output_string)
        self.my_logger.info(output_string)

    def create_logger(self):
        """ Creating logger """
        path = os.getcwd()
        config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
        self.my_logger = XprLogger(config_path=config_file)

    def main(self):
        """ Main method"""

        # Creating logger
        self.create_logger()

        self.my_logger.info("Process started")
        print("Process started")

        # Computing ETA
        self.get_estimated_time()

        # Prepare test data
        self.my_logger.info("Prepare test data")
        print("Prepare test data")
        self.load_test_file()

        # Getting hit raio of the test data
        self.my_logger.info("Getting hit ratio of the test data")
        print("Getting hit ratio of the test data")
        self.test(self.neighbors)


# Creating class object
CLASS_OBJ = BNetworkTest()

# Calling main method of class
CLASS_OBJ.main()
