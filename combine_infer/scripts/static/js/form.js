$(document).ready(function() {

	$('form').on('submit', function(event) {
        $('#output').val('')
		$.ajax({
			data : {
				inputseq : $('#InputSequence').val(),
				topk : $('#TopkNumber').val(),
				model : $('#modelSelection').val()
			},
			type : 'POST',
			url : '/combine_infer'
		})
		.done(function(data) {

			if (data.error) {
				$('#output').val(data.error)
			}
			else {
			    $('#output').val(data.prediction)
			}

		});

		event.preventDefault();

	});

});