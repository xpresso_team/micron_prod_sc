import os
from app.model_name_gen import create_modelname, get_master_path
import json
import time
import fasttext

cur_work_dir = os.getcwd()

tic = time.time()

print("Started training model")

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))
ft_config = config['model']['fasttext']
master_path = get_master_path(config)

train_file_name = os.path.abspath(
    os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path, './data/train_seq.txt'))

model_name_temp = "model_" + create_modelname(ft_config) + '.bin'
model_name = os.path.abspath(os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                                          str(config['fasttext']['model_save_path']), str(model_name_temp)))

algo = ft_config['algo']

# CBOW model
if algo == 'CBOW':
    model = fasttext.train_unsupervised(train_file_name, model='cbow')
    model.save_model(model_name)

# Skipgram model
if algo == 'SKIPGRAM':
    model = fasttext.train_unsupervised(train_file_name, model='skipgram')
    model.save_model(model_name)

toc = time.time()

print("Completed training model", toc - tic)
