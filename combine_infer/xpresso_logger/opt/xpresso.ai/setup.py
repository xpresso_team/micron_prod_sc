from setuptools import setup

# Setup configuration
setup(
    name='xpresso_logger',
    version='1.2.4',
    packages=['xpresso'],
    description="Xpresso Logger to be used with Python components",
    author="Naveen Sinha",
    author_email="naveen.sinha@abzooba.com",
    install_requires=['python-logstash-async>=1.5.1']
)
