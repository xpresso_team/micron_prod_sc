def create_filename(config):
    return ("perplexity" + str(config['tsne']['perplexity']) + "_cores_" + str(config['tsne']['cores']) +
            "_n_components_" + str(config['tsne']['n_components']) + "_iter_" + str(config['tsne']['iter']))
