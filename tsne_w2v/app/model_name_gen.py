def create_modelname(config):
    name = ''
    for key in config:
        name = name + key + '_' + str(config[key]) + '_'
    return name


def get_master_path(config):
    filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
    master_path = config['data_preprocess']['master_path']
    master_path = master_path.replace("FILE", filename)
    master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
    master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
    master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
    master_path = str(master_path)
    return master_path
