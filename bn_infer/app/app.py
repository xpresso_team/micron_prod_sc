""" Probability graph API module """
import json
import logging
import os
import pickle
from flask import Flask
from flask_restful import Resource, Api, reqparse
from xpresso.ai.core.logging.xpr_log import XprLogger

APP = Flask(__name__)
API = Api(APP)


# Model loading function
def load_model(filename):
    """ Loading graph """
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


# Getting model data
def load_test_file_helper():
    """ helper function for test sequence generation """
    master_dict = {}
    for item in RANGE_LIST:
        MY_LOGGER.info("Loading file %s", str(item))
        print("Loading file", str(item))
        filename = BN_SPLIT.replace('VAL', str(item))
        master_dict[str(item)] = load_model(filename)
    return master_dict


# Getting prediction
def get_pred(key_val, item3):
    """ Getting prediction of sequence """
    pred = []
    key = '_'.join([str(item2) for item2 in key_val])
    if key in MODELDATA[item3]:
        pred = MODELDATA[item3][key]
    return pred


# Getting final prediction
def get_model_output(block, neighbor):
    """ Get sequence and return the predicted sequence """
    if len(block) < LOOKAHEAD:
        block = [-1] * (LOOKAHEAD - len(block)) + block
    else:
        block = block[-LOOKAHEAD:]

    predict = []
    for item in RANGE_LIST:
        key = list(block)[-item:]
        predict += get_pred(key, str(item))
        if len(predict) >= neighbor:
            break
    return predict[:neighbor]


# Logger untility function
def create_logger():
    """ Creating logger """
    path = os.getcwd()
    config_file = os.path.join(path, 'config', 'xpr_log_stage.json')
    my_logger = XprLogger(config_path=config_file)
    return my_logger


# Probability graph class for GUI
class BNetworkPredict(Resource):
    """ Declaring and defining class objects """

    def post(self):
        """ Method for POST response """
        try:
            PARSER.add_argument('Topk', type=str)
            PARSER.add_argument('InputSequence', type=str)
            args = PARSER.parse_args()
            block = args['InputSequence'].split(' ')
            neighbor = int(args['Topk'])
            MY_LOGGER.info('Input %s', block)
            MY_LOGGER.info('TopK %s', neighbor)
            print(args)
            final_output = get_model_output(block, neighbor)
            MY_LOGGER.info('Output %s', final_output)
            print(f'Block_Sequence: {final_output}')
            return {'Block_Sequence': final_output}, 200
        except Exception:
            final_output = "Internal Server Error"
            MY_LOGGER.info('Output %s', final_output)
            return final_output, 500


PARSER = reqparse.RequestParser()
API.add_resource(BNetworkPredict, '/bn_predict_block')

if __name__ == '__main__':
    # Reading config data from json
    CONFIG_PATH = 'config/config.json'
    CONFIG_FILE = open(CONFIG_PATH, 'r')
    JSON_OBJECT = json.load(CONFIG_FILE)
    CONFIG_FILE.close()

    DATA_MODEL = JSON_OBJECT['model']['pg_bn']
    DATA_PREP_PARAM = JSON_OBJECT['data_preprocess']

    INPUTFILE = DATA_PREP_PARAM['input_file']

    READ_WRITE_FLAG = DATA_PREP_PARAM['use_read_write_operation_feature']
    SPLITPERC = DATA_PREP_PARAM['train_split_ratio']
    CUTTING_WINDOW = DATA_PREP_PARAM['cutting_window']

    FILENAME = str(INPUTFILE.split('/')[-1].split('.')[0])
    MASTERPATH = DATA_PREP_PARAM['master_path']
    MASTERPATH = MASTERPATH.replace("FILE", FILENAME)
    MASTERPATH = MASTERPATH.replace("CUTTING", str(CUTTING_WINDOW))
    MASTERPATH = MASTERPATH.replace("SPLIT", str(SPLITPERC))
    MASTERPATH = MASTERPATH.replace("RW", str(READ_WRITE_FLAG))
    MOUNTEDPATH = DATA_PREP_PARAM['mounted_path'] + MASTERPATH
    MODELPATH = MOUNTEDPATH + DATA_MODEL['model_path_bn']

    LOGPATH = MODELPATH + DATA_PREP_PARAM['logfile_inference']
    LOOKAHEAD = DATA_MODEL['lookahead_window']
    RANGE_LIST = range(LOOKAHEAD, 0, -1)
    BN_SPLIT = MODELPATH + DATA_MODEL['output_files']['bn_split']

    MY_LOGGER = create_logger()

    MY_LOGGER.info("Process started")
    print("Process started")

    MY_LOGGER.info("Model loading... ")
    print("Model loading... ")
    MODELDATA = load_test_file_helper()
    MY_LOGGER.info("Model loading Completed")
    print("Model loading Completed")

    HOSTNAME = DATA_MODEL['hostname']
    PORT = DATA_MODEL['bn_api_port']
    APP.run(host=HOSTNAME, port=PORT)
